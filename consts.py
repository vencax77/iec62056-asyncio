'''
Uncomment to override automatic mode selection, for example to limit the baud
rate. Use if you have problems with your optical receiver.
'''
#MODE_OVERRIDE '4'

READ_DELAY = 1000 # ms

MAX_OBIS_CODE_LENGTH = 16
MAX_IDENTIFICATION_LENGTH = 5 + 16 + 1 # /AAAbi...i\r
MAX_VALUE_LENGTH = 32 + 1 + 16 + 1     # value: 32, *, unit: 16
MAX_LINE_LENGTH = 78

INITIAL_BAUD_RATE = 300

STX = b'\x02'
ETX = b'\x03'

ACK = b"\x06"

BAUD_RATES = [
    300,        # 0
    600,        # 1, A
    1200,       # 2, B
    2400,       # 3, C
    4800,       # 4, D
    9600,       # 5, E
    19200       # 6, F
]

def baud_char_to_params(id):
	if id >= '0' and id <= '6':     # Mode C
        # send acknowledgement, switch baud
		return {
            'ack': True, 
            'new_baud': BAUD_RATES[id - '0']
        }
	elif id >= 'A' and id <= 'F':   # Mode B
        # no acknowledgement, switch baud
		return {
            'ack': False, 
            'new_baud': BAUD_RATES[id - 'A' + 1]
        }
	else:  # possibly Mode A
        # no acknowledgement, don't switch baud
		return {'ack': False, 'new_baud': None}