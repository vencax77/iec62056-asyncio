import consts

def discardLog(s): pass

def readLineFn(destbytearray, maxlen, uart):
    cntr = 0
    while True:
        if uart.any() > 0:
            c = uart.read(1)
            destbytearray[cntr] = c[0]
            cntr += 1
            if c[0] == ord('\n') or cntr == maxlen:
                break

def read(uart, processDataline, opts={}):
    debug = opts["debugFn"] if "debugFn" in opts else discardLog
    # NOTE: serial must be 7 data bits, even parity, 1 stop bit
    uart.init(consts.INITIAL_BAUD_RATE, bits=7, parity=0, stop=1, timeout=2000, timeout_char=500)
    # step 1 -send init sequence
    uart.write("/?!\r\n")
    uart.flush()

    line = bytearray(consts.MAX_LINE_LENGTH)
    # step 2 - read meter ID and baud request #####################
    readLineFn(line, consts.MAX_IDENTIFICATION_LENGTH, uart)
    debug("identification=%s" % line)
    # id[4] is id of speed (see consts.BAUD_RATES), that meter suggest to use
    baud_char = opts['baudOverride'] if 'baudOverride' in opts else line[4]

    # step 3 - possibly ack and modify baudrate ###################
    params = consts.baud_char_to_params(baud_char)
    debug(str(params))

    if params['ack']:
        uart.write(consts.ACK)
        uart.write("0%c0\r\n" % baud_char)
        uart.flush()

    if params['new_baud'] and params['new_baud'] != consts.INITIAL_BAUD_RATE:
        debug("switching to %s bps" % params['new_baud'])
        uart.init(params['new_baud'], bits=7, parity=0, stop=1, timeout=2000, timeout_char=500)

    # step 4 - read actual data messages ##########################
    while uart.any() == 0: pass # wait
    begindata = uart.read(1)
    if begindata != consts.STX: raise Exception('STX expected')
    readLineFn(line, consts.MAX_LINE_LENGTH, uart)
    while line[0] != ord('!'):
        endidx = line.find(b'\n')
        processDataline(line[:endidx].decode("utf-8"))
        readLineFn(line, consts.MAX_LINE_LENGTH, uart)
    readLineFn(line, 2, uart)
    if line[0] != consts.ETX: raise Exception('ETX expected')
    debug(line)


def read_line(uart):
    line = _readBytesUntil('\n', consts.MAX_LINE_LENGTH, uart)
    # Start with checksum=STX to avoid having to avoid xoring it
    checksum = consts.STX
    debug("line: %s" % line)
    debug("checksum: %s" % checksum)

    while line[len(line) - 2] != '!':
        for ch in line:
            checksum ^= ch
        # readBytesUntil doesn't include the terminator, so take it into account separately
        checksum ^= '\n'
        line = _readBytesUntil('\n', consts.MAX_LINE_LENGTH, uart)
        debug("line: %s" % line)
    
    for ch in line:
        checksum ^= ch
    # readBytesUntil doesn't include the terminator, so take it into account separately
    checksum ^= '\n'


def verify_checksum(checksum): pass
    # Expecting ETX and then the checksum
    # if(serial_.readBytes(etx_bcc, 2) != 2 || etx_bcc[0] != ETX)
    # {
    # 	logger::err("failed to read checksum");
    # 	return change_status(Status::ProtocolError);
    # }

    # checksum ^= ETX
    # if(checksum_ != etx_bcc[1])
    # {
    # 	logger::err("checksum mismatch: %02" PRIx8 " != %02" PRIx8, checksum_, etx_bcc[1]);
    # 	return change_status(Status::ChecksumError);
    # }
